import React from 'react';
import ReactDOM from "react-dom";
import Form from "./components/Form";

const App = () => 
<>
    <Form />
</>;

const root = document.getElementById("app-root");

ReactDOM.render(<App/>, root);
